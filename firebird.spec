%global         upversion 3.0.1.32609
%global         pkgversion Firebird-%{upversion}-0

%global         major 3.0
%global         _hardened_build 1

Name:           firebird
Version:        %{upversion}
Release:        2%{?dist}

Summary:        SQL relational database management system
Group:          Applications/Databases
License:        Interbase
URL:            http://www.firebirdsql.org/
BuildRoot:      %{_tmppath}/%{name}-%{version}-buildroot

Source0:        %{pkgversion}.tar.bz2
Source1:        firebird-logrotate
Source2:        README.Fedora
Source3:        firebird.conf
Source4:        fb_config

# from OpenSuse
Patch101:       add-pkgconfig-files.patch
Patch102:       Make-the-generated-code-compatible-with-gcc-6-in-C-1.patch
Patch103:       Provide-sized-global-delete-operators-when-compiled.patch

# from Debian to be sent upstream
Patch201:       obsolete-syslogd.target.patch
Patch202:       honour-buildflags.patch
Patch203:       no-copy-from-icu.patch
Patch205:       cloop-honour-build-flags.patch

# from upstream

BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtommath-devel
BuildRequires:  libtool
BuildRequires:  ncurses-devel
BuildRequires:  libtermcap-devel
BuildRequires:  libicu-devel
BuildRequires:  libedit-devel
BuildRequires:  gcc-c++
BuildRequires:  libstdc++-static
BuildRequires:  systemd-units
BuildRequires:  chrpath
BuildRequires:  zlib-devel
BuildRequires:  procmail

Requires(post):   /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Requires(postun): /usr/sbin/userdel
Requires(postun): /usr/sbin/groupdel
Requires(pre):    /usr/sbin/groupadd
Requires(pre):    /usr/sbin/useradd
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units
Requires:       logrotate
Requires:       libfbclient2 = %{version}-%{release}
Requires:       libib-util = %{version}-%{release}
Requires:       %{name}-utils = %{version}-%{release}

Obsoletes:      firebird-arch < 3.0
Obsoletes:      firebird-filesystem < 3.0
Obsoletes:      firebird-classic-common < 3.0
Obsoletes:      firebird-classic < 3.0
Obsoletes:      firebird-superclassic < 3.0
Obsoletes:      firebird-superserver < 3.0
Conflicts:      firebird-arch < 3.0
Conflicts:      firebird-filesystem < 3.0
Conflicts:      firebird-classic-common < 3.0
Conflicts:      firebird-classic < 3.0
Conflicts:      firebird-superclassic < 3.0
Conflicts:      firebird-superserver < 3.0


%description
Firebird is a relational database offering many ANSI SQL standard
features that runs on Linux, Windows, and a variety of Unix platforms.
Firebird offers excellent concurrency, high performance, and powerful
language support for stored procedures and triggers. It has been used
in production systems, under a variety of names, since 1981.


%package devel
Group:          Development/Libraries/C and C++
Requires:       %{name} = %{version}-%{release}
Summary:        UDF support library for Firebird SQL server

%description devel
This package is needed for development of client applications and user
defined functions (UDF) for Firebird SQL server.

Firebird is a relational database offering many ANSI SQL standard
features that runs on Linux, Windows, and a variety of Unix platforms.
Firebird offers excellent concurrency, high performance, and powerful
language support for stored procedures and triggers. It has been used
in production systems, under a variety of names, since 1981.


%package -n libib-util
Group:          System Environment/Libraries
Summary:        Firebird SQL UDF support library

%description -n libib-util
libib_util contains utility functions used by
User-Defined Functions (UDF) for memory management etc.

Firebird is a relational database offering many ANSI SQL standard
features that runs on Linux, Windows, and a variety of Unix platforms.
Firebird offers excellent concurrency, high performance, and powerful
language support for stored procedures and triggers. It has been used
in production systems, under a variety of names, since 1981.


%package -n libfbclient2
Group:          System Environment/Libraries
Summary:        Firebird SQL server client library
Requires(post):   /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Obsoletes:      firebird-libfbclient < 3.0
Conflicts:      firebird-libfbclient < 3.0

%description -n libfbclient2
Shared client library for Firebird SQL server.

Firebird is a relational database offering many ANSI SQL standard
features that runs on Linux, Windows, and a variety of Unix platforms.
Firebird offers excellent concurrency, high performance, and powerful
language support for stored procedures and triggers. It has been used
in production systems, under a variety of names, since 1981.


%package -n libfbclient2-devel
Group:          System Environment/Libraries
Summary:        Development libraries and headers for Firebird SQL server
Requires:       %{name}-devel = %{version}-%{release}
Requires:       libfbclient2 = %{version}-%{release}
Requires:       pkgconfig

%description -n libfbclient2-devel
Development files for Firebird SQL server client library.

Firebird is a relational database offering many ANSI SQL standard
features that runs on Linux, Windows, and a variety of Unix platforms.
Firebird offers excellent concurrency, high performance, and powerful
language support for stored procedures and triggers. It has been used
in production systems, under a variety of names, since 1981.


%package doc
Group:          Applications/Databases
Requires:       %{name} = %{version}-%{release}
Summary:        Documentation for Firebird SQL server
BuildArch:      noarch

%description doc
Documentation for Firebird SQL server.

Firebird is a relational database offering many ANSI SQL standard
features that runs on Linux, Windows, and a variety of Unix platforms.
Firebird offers excellent concurrency, high performance, and powerful
language support for stored procedures and triggers. It has been used
in production systems, under a variety of names, since 1981.


%package utils
Group:          Applications/Databases
Requires:       libfbclient2 = %{version}-%{release}
Summary:        Firebird SQL user utilities

%description utils
Firebird SQL user utilities.

Firebird is a relational database offering many ANSI SQL standard
features that runs on Linux, Windows, and a variety of Unix platforms.
Firebird offers excellent concurrency, high performance, and powerful
language support for stored procedures and triggers. It has been used
in production systems, under a variety of names, since 1981.


%package examples
Group:          Applications/Databases
Requires:       %{name}-doc = %{version}-%{release}
Summary:        Examples for Firebird SQL server
BuildArch:      noarch

%description examples
Examples for Firebird SQL server.

Firebird is a relational database offering many ANSI SQL standard
features that runs on Linux, Windows, and a variety of Unix platforms.
Firebird offers excellent concurrency, high performance, and powerful
language support for stored procedures and triggers. It has been used
in production systems, under a variety of names, since 1981.


%prep
%setup -q -n %{pkgversion}
%patch101 -p1
%patch102 -p1
%patch103 -p1
%patch201 -p1
%patch202 -p1
%patch203 -p1
%patch205 -p1


%build
export CFLAGS="%{optflags} -fno-strict-aliasing"
export CXXFLAGS="${CFLAGS} -fno-delete-null-pointer-checks"
NOCONFIGURE=1 ./autogen.sh
%configure --prefix=%{_prefix} \
  --disable-binreloc \
  --with-system-editline \
  --with-fbbin=%{_bindir} --with-fbsbin=%{_sbindir} \
  --with-fbconf=%{_sysconfdir}/%{name} \
  --with-fblib=%{_libdir} --with-fbinclude=%{_includedir}/%{name} \
  --with-fbdoc=%{_defaultdocdir}/%{name} \
  --with-fbudf=%{_libdir}/%{name}/udf \
  --with-fbsample=%{_defaultdocdir}/%{name}/sample \
  --with-fbsample-db=%{_localstatedir}/lib/%{name}/data/ \
  --with-fbhelp=%{_localstatedir}/lib/%{name}/system/ \
  --with-fbintl=%{_libdir}/%{name}/intl \
  --with-fbmisc=%{_datadir}/%{name}/misc \
  --with-fbsecure-db=%{_localstatedir}/lib/%{name}/secdb/ \
  --with-fbmsg=%{_localstatedir}/lib/%{name}/system/ \
  --with-fblog=%{_localstatedir}/log/%{name} \
  --with-fbglock=%{_var}/run/%{name} \
  --with-fbplugins=%{_libdir}/%{name}/plugins

make %{?_smp_mflags}
cd gen
make -f Makefile.install buildRoot
chmod -R u+w buildroot%{_docdir}/%{name}

%install
chmod u+rw,a+rx gen/buildroot/usr/include/firebird/firebird/impl
cp -r gen/buildroot/* ${RPM_BUILD_ROOT}/
mkdir -p ${RPM_BUILD_ROOT}%{_libdir}/pkgconfig
cp -v gen/install/misc/*.pc ${RPM_BUILD_ROOT}%{_libdir}/pkgconfig/

cd ${RPM_BUILD_ROOT}
rm -vf .%{_sbindir}/*.sh
mv -v .%{_sbindir}/fb_config .%{_libdir}/
install -p -m 0755 %{SOURCE4} %{buildroot}%{_sbindir}/fb_config
rm -vf .%{_includedir}/%{name}/perf.h
rm -vf .%{_libdir}/libicu*.so
rm -vf .%{_includedir}/*.h
chmod -R u+w .%{_docdir}/%{name}
rm -vf .%{_datadir}/%{name}/misc/firebird.init.*
rm -vf .%{_datadir}/%{name}/misc/firebird.xinetd
rm -vf .%{_datadir}/%{name}/misc/rc.config.firebird
mv -v .%{_sysconfdir}/%{name}/README .%{_sysconfdir}/%{name}/WhatsNew \
  .%{_docdir}/%{name}/
mv -v .%{_sysconfdir}/%{name}/IDPLicense.txt .%{_docdir}/%{name}/
mv -v .%{_sysconfdir}/%{name}/IPLicense.txt .%{_docdir}/%{name}/
install -p -m 0644 -D %{SOURCE2} .%{_docdir}/%{name}/README.Fedora
mv -v .%{_bindir}/gstat .%{_bindir}/gstat-fb
mv -v .%{_bindir}/isql .%{_bindir}/isql-fb

mkdir -p .%{_localstatedir}/log/%{name}
mkdir -p .%{_sysconfdir}/logrotate.d
echo 1 > .%{_localstatedir}/log/%{name}/%{name}.log
sed "s@%{name}.log@%{_localstatedir}/log/%{name}/%{name}.log@g" %{SOURCE1} > .%{_sysconfdir}/logrotate.d/%{name}

mkdir -p .%{_tmpfilesdir}
cp %{SOURCE3} .%{_tmpfilesdir}/

mkdir -p .%{_unitdir}
cp .%{_datadir}/%{name}/misc/%{name}-superserver.service .%{_unitdir}/%{name}-superserver.service


%clean
rm -Rf %{buildroot}

%post -n libfbclient2 -p /sbin/ldconfig


%postun -n libfbclient2 -p /sbin/ldconfig


%pre 
# Create the firebird group if it doesn't exist
getent group %{name} || /usr/sbin/groupadd -r %{name} 
getent passwd %{name} >/dev/null || /usr/sbin/useradd -d / -g %{name} -s /bin/nologin -r %{name} 

# Add gds_db to /etc/services if needed
FileName=/etc/services
newLine="gds_db 3050/tcp  # Firebird SQL Database Remote Protocol"
oldLine=`grep "^gds_db" $FileName`
if [ -z "$oldLine" ]; then
 echo $newLine >> $FileName
fi


%post 
/sbin/ldconfig
/bin/systemd-tmpfiles --create  %{_sysconfdir}/tmpfiles.d/firebird.conf
%systemd_post firebird-superserver.service


%postun 
/sbin/ldconfig
%systemd_postun_with_restart firebird-superserver.service


%preun 
%systemd_preun firebird-superserver.service


%files
%{_docdir}/%{name}/IDPLicense.txt
%{_docdir}/%{name}/IPLicense.txt
%{_docdir}/%{name}/README.Fedora
%{_bindir}/fbtracemgr
%{_sbindir}/firebird
%{_sbindir}/fbguard
%{_sbindir}/fb_lock_print
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/databases.conf
%config(noreplace) %{_sysconfdir}/%{name}/fbtrace.conf
%config(noreplace) %{_sysconfdir}/%{name}/firebird.conf
%config(noreplace) %{_sysconfdir}/%{name}/plugins.conf
%dir %{_libdir}/%{name}
%dir %{_datadir}/%{name}
%{_libdir}/%{name}/intl
%{_libdir}/%{name}/plugins
%{_libdir}/%{name}/udf
%{_datadir}/%{name}/misc

%dir %{_localstatedir}/lib/%{name}
%dir %attr(0700,%{name},%{name}) %{_localstatedir}/lib/%{name}/secdb
%dir %attr(0700,%{name},%{name}) %{_localstatedir}/lib/%{name}/data
%dir %attr(0755,%{name},%{name}) %{_localstatedir}/lib/%{name}/system
%attr(0600,firebird,firebird) %config(noreplace) %{_localstatedir}/lib/%{name}/secdb/security3.fdb
%attr(0644,firebird,firebird) %{_localstatedir}/lib/%{name}/system/help.fdb
%attr(0644,firebird,firebird) %{_localstatedir}/lib/%{name}/system/firebird.msg
%ghost %dir %attr(0775,%{name},%{name}) %{_var}/run/%{name}
%ghost %attr(0644,%{name},%{name}) %{_var}/run/%{name}/fb_guard
%attr(0644,root,root) %{_tmpfilesdir}/firebird.conf 
%dir %{_localstatedir}/log/%{name}
%config(noreplace) %attr(0664,%{name},%{name})  %{_localstatedir}/log/%{name}/%{name}.log
%config(noreplace) %attr(0644,root,root) %{_sysconfdir}/logrotate.d/%{name}

%defattr(0755,root,root,0755)
%{_unitdir}/%{name}-superserver.service


%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/fb_config
%{_sbindir}/fb_config


%files -n libfbclient2
%defattr(-,root,root)
%{_libdir}/libfbclient.so.*


%files -n libfbclient2-devel
%defattr(-,root,root)
%{_libdir}/libfbclient.so
%{_libdir}/pkgconfig/fbclient.pc


%files -n libib-util
%defattr(-,root,root)
%{_libdir}/libib_util.so


%files doc
%{_docdir}/%{name}
%exclude %{_docdir}/%{name}/sample
%exclude %{_docdir}/%{name}/IDPLicense.txt
%exclude %{_docdir}/%{name}/IPLicense.txt


%files utils
%defattr(-,root,root)
%{_bindir}/gstat-fb
%{_bindir}/fbsvcmgr
%{_bindir}/gbak
%{_bindir}/gfix
%{_bindir}/gpre
%{_bindir}/gsec
%{_bindir}/isql-fb
%{_bindir}/nbackup
%{_bindir}/qli
%{_bindir}/gsplit


%files examples
%{_docdir}/%{name}/sample
%attr(0600,firebird,firebird) %{_localstatedir}/lib/%{name}/data/employee.fdb


%changelog
* Fri Sep 30 2016 Philippe Makowski <makowski@fedoraproject.org> - 3.0.1.32609-2
- fix perms
- change build option for Fedora and gcc

* Fri Sep 30 2016 Philippe Makowski <makowski@fedoraproject.org> - 3.0.1.32609-1
- new upstream release

* Fri Apr 15 2016 Philippe Makowski <makowski@fedoraproject.org> - 3.0.0.32483-1
- new upstream release

* Fri Apr 08 2016 Philippe Makowski <makowski@fedoraproject.org> - 3.0.0.32366-3
- fix wrong source number for README.Fedora

* Thu Mar 31 2016 Philippe Makowski <makowski@fedoraproject.org> - 3.0.0.32366-2
- use _tmpfilesdir macro

* Tue Mar 1 2016 Philippe Makowski <makowski@fedoraproject.org> - 3.0.0.32366-1
-  first version for Epel7

